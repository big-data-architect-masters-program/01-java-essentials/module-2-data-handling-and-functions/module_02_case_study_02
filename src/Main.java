import java.util.Scanner;

public class Main {

    public static void main(String[] args) {
        Phone p = new Phone();
        Scanner sc = new Scanner(System.in);
        System.out.println("1: Call, 2: Receive Call, 3: Message");
        System.out.print("Select function:");
        int fn = sc.nextInt();
        if (fn == 1) {
            System.out.print("Call to Number:");
            String num = sc.next();
            p.call(num);
        } else if (fn == 2) {
            p.receive();
        } else {
            System.out.print("Number:");
            String num = sc.next();
            System.out.print("Message:");
            String msg = sc.next();
            p.message(num, msg);
        }
    }
}

class Phone {
    void call(String number) {
        System.out.println("Calling " + number + "...");
    }

    void receive() {
        Scanner sc = new Scanner(System.in);
        System.out.println("You receiving call from 012333444...");
        System.out.println("Press 1: Answer, 2: Reject");
        int fn = sc.nextInt();
        if (fn == 1) {
            System.out.println("You are connected!...");
        } else {
            System.out.println("Call has been rejected!...");
        }
    }

    void message(String number, String msg) {
        System.out.println("Sending " + number + ":");
        System.out.println(msg);
        System.out.println("Sent!");
    }
}